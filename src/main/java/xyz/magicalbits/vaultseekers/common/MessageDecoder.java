/**
 * common
 * Copyright (C) 2022  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package xyz.magicalbits.vaultseekers.common;


import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.CorruptedFrameException;
import java.util.Arrays;
import java.util.List;
import xyz.magicalbits.vaultseekers.common.proto.Message;

public class MessageDecoder extends ByteToMessageDecoder {
  @Override
  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
    // Wait until the length prefix is available.
    if (in.readableBytes() < 7) {
      return;
    }

    in.markReaderIndex();

    // Check the magic number.
    byte[] magicNumber = new byte[3];
    in.readBytes(magicNumber, 0, 3);
    if (!Arrays.equals(magicNumber, new byte[] {'M', 'S', 'G'})) {
      in.resetReaderIndex();
      throw new CorruptedFrameException("Invalid magic number: " + Arrays.toString(magicNumber));
    }

    // Wait until the whole data is available.
    int dataLength = in.readInt();
    if (in.readableBytes() < dataLength) {
      in.resetReaderIndex();
      return;
    }

    // Parse incoming bytes to a Message object.
    out.add(Message.parseFrom(in.readBytes(dataLength).nioBuffer()));
  }
}
