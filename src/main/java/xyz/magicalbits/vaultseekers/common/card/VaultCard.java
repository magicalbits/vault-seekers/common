/**
 * common
 * Copyright (C) 2022  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package xyz.magicalbits.vaultseekers.common.card;


import com.badlogic.gdx.scenes.scene2d.ui.Image;
import lombok.Getter;
import lombok.ToString;
import xyz.magicalbits.vaultseekers.common.proto.Gem;

@ToString(callSuper = true)
public class VaultCard extends Card {
  @Getter private Gem.Color color;

  public VaultCard(Image image) {
    super(image);
  }

  VaultCard(Image image, Gem.Color color) {
    super(image);
    this.color = color;
  }

  public VaultCard(String filePath) {
    super(filePath);
  }

  VaultCard(String filePath, Gem.Color color) {
    super(filePath);
    this.color = color;
  }
}
