/**
 * common
 * Copyright (C) 2022  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package xyz.magicalbits.vaultseekers.common.card;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.magicalbits.vaultseekers.common.Location;
import xyz.magicalbits.vaultseekers.common.proto.Message;

@ToString(callSuper = true)
public class PlayingCard extends Card {
  @Getter private final Message.PlayerColor color;
  public final Location.Name name;
  @Getter @Setter public State state;

  public enum State {
    IN_HAND,
    ATTACK,
    DEFENSE
  }

  public PlayingCard(String path, Message.PlayerColor color, Location.Name name, State state) {
    this(path, color, name);
    this.state = state;
  }

  public PlayingCard(String path, Message.PlayerColor color, Location.Name name) {
    super(new Image(new Texture(path)), path);
    this.color = color;
    this.name = name;
  }
}
