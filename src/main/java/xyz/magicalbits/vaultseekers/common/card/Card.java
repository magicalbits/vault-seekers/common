/**
 * common
 * Copyright (C) 2022  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package xyz.magicalbits.vaultseekers.common.card;


import com.badlogic.gdx.scenes.scene2d.ui.Image;
import lombok.Getter;
import lombok.ToString;

@ToString
public abstract class Card {
  @Getter private Image image;
  @Getter private String filePath;

  protected boolean isVisible = true;

  // Client-side constructor only. Used by class `VaultCard`.
  // Note: clients do _not_ need to store VaultCard paths. They only receive them from the server
  // to create and place VaultCards.
  protected Card(Image image) {
    this.image = image;
  }

  // Client-side constructor only. Used by class `PlayingCard`.
  // Note: clients _do_ need PlayingCard paths because they send them to the server when they end
  // their turn.
  protected Card(Image image, String filePath) {
    this.image = image;
    this.filePath = filePath;
  }

  // Server-side constructor only.
  protected Card(String filePath) {
    this.filePath = filePath;
  }

  public void hide() {
    if (image.isVisible()) {
      image.setVisible(false);
    }
  }

  public void show() {
    if (!image.isVisible()) {
      image.setVisible(true);
    }
  }
}
