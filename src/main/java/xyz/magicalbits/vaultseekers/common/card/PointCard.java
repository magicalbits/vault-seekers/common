/**
 * common
 * Copyright (C) 2022  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package xyz.magicalbits.vaultseekers.common.card;


import com.badlogic.gdx.scenes.scene2d.ui.Image;
import lombok.Getter;

public class PointCard extends VaultCard {
  @Getter final int points;

  public PointCard(Image image, int points) {
    super(image);
    this.points = points;
  }

  public PointCard(String filePath, int points) {
    super(filePath);
    this.points = points;
  }
}
