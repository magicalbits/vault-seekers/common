/**
 * common
 * Copyright (C) 2022  MagicalBits / vault-seekers
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package xyz.magicalbits.vaultseekers.common;


import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.magicalbits.vaultseekers.common.card.LocationCard;
import xyz.magicalbits.vaultseekers.common.card.PlayingCard;
import xyz.magicalbits.vaultseekers.common.card.VaultCard;

@Getter
@ToString
public class Location {
  private final Name name;
  private final LocationCard locationCard;
  @Setter private List<PlayingCard> defenseCards;
  @Setter private List<PlayingCard> attackCards;
  @Setter private VaultCard vaultCard;

  public Location(Name name, LocationCard locationCard) {
    this.name = name;
    this.locationCard = locationCard;
  }

  // TODO: A-F for 2-3 players, A-G for 4 players, A-H for 5 players
  public enum Name {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H
  }
}
